from dbconn import Departments, Employees
from dbconn import Session

session = Session()

##########################################
hr = Departments(dep_id=1, dep_name='人事部')
finance = Departments(dep_id=2, dep_name='财务部')
ops = Departments(dep_id=3, dep_name='运维部')
dev = Departments(dep_id=4, dep_name='开发部')
qa = Departments(dep_id=5, dep_name='测试部')
market = Departments(dep_id=6, dep_name='市场部')
deps = [hr, finance, ops, dev, qa, market]
session.add_all(deps)
##########################################
ly = Employees(
    emp_id=1,
    emp_name='刘宇',
    email='liuyu@feizhi.com',
    dep_id=1
)
#######################################
qset1 = session.query(Employees)
print(qset1)
for emp in qset1:
    print('%s: %s, %s' % (emp.emp_id, emp.emp_name, emp.email))

qset2 = session.query(Departments)
for dep in qset2:
    print('%s: %s' % (dep.dep_id, dep.dep_name))

qset3 = session.query(Departments).order_by(Departments.dep_id)
for dep in qset3:
    print('%s: %s' % (dep.dep_id, dep.dep_name))

qset4 = session.query(Employees.emp_name, Employees.email)
print(qset4.all())

qset5 = session.query(Departments).order_by(Departments.dep_id)[3:]
print(qset5.all())

qset6 = session.query(Departments).filter(Departments.dep_id>3)
print(qset6.all())

qset7 = session.query(Departments).filter(Departments.dep_id>3).filter(Departments.dep_id<6)
print(qset7.all())

qset8 = session.query(Employees).filter(Employees.email.like('%@feizhi.com'))
print(qset8.all())

qset9 = session.query(Departments).filter(Departments.dep_id.in_([2, 5]))
print(qset9.all())

qset10 = session.query(Departments).filter(Departments.dep_name.isnot(None))
print(qset10.all())

qset11 = session.query(Employees.emp_name, Departments.dep_name).join(Departments)
print(qset11.all())

qset12 = session.query(Departments.dep_name, Employees.emp_name).join(Employees)
print(qset12.all())

qset13 = session.query(Departments).filter(Departments.dep_id==1)
print(qset13.all())

qset14 = session.query(Departments).filter(Departments.dep_id==1)
hr = qset14.one()
hr.dep_name = '人力资源部'

qset15 = session.query(Departments).filter(Departments.dep_name=='市场部')
market = qset15.one()
session.delete(market)



session.commit()
session.close()
