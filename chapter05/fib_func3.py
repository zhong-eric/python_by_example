def gen_fib(length):
    fib = [0, 1]

    for i in range(length - 2):
        fib.append(fib[-1] + fib[-2])

    return fib

result = gen_fib(5)
print(result)
