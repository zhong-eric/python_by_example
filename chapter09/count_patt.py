import re

def count_patt(fname, patt):
    result = {}
    cpatt = re.compile(patt)  # 编译模式

    with open(fname) as fobj:
        for line in fobj:
            m = cpatt.search(line)
            if m:  # 匹配到的匹配对象非空为真，未匹配到是None为假
                key = m.group()
                result[key] = result.get(key, 0) + 1

    return result

if __name__ == '__main__':
    fname = 'access_log'
    ip = '^(\d+\.){3}\d+'
    br = 'Firefox|Chrome|MSIE'
    print(count_patt(fname, ip))
    print(count_patt(fname, br))
