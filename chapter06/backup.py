import os
import tarfile
import hashlib
import pickle
from time import strftime


def check_md5(fname):
    '接收文件名，返回文件的md5值'
    m = hashlib.md5()

    with open(fname, 'rb') as fobj:
        while True:
            data = fobj.read(4096)
            if not data:
                break
            m.update(data)

    return m.hexdigest()


def full_backup(src_dir, dst_dir, md5file):
    # 拼接目标文件的绝对路径
    fname = os.path.basename(src_dir.rstrip('/'))
    fname = '%s_full_%s.tar.gz' % (fname, strftime('%Y%m%d'))
    fname = os.path.join(dst_dir, fname)
    # 创建用于保存md5值的字典
    md5dict = {}

    # 把整个目录打包压缩
    tar = tarfile.open(fname, 'w:gz')
    tar.add(src_dir)
    tar.close()

    # 计算每个文件的md5值，存入字典
    for path, folders, files in os.walk(src_dir):
        for each_file in files:
            key = os.path.join(path, each_file)
            md5dict[key] = check_md5(key)

    # 将字典通过pickle存到文件中
    with open(md5file, 'wb') as fobj:
        pickle.dump(md5dict, fobj)


def incr_backup(src_dir, dst_dir, md5file):
    fname = os.path.basename(src_dir.rstrip('/'))
    fname = '%s_incr_%s.tar.gz' % (fname, strftime('%Y%m%d'))
    fname = os.path.join(dst_dir, fname)
    md5dict = {}

    # 取出前一天文件的md5值字典
    with open(md5file, 'rb') as fobj:
        oldmd5 = pickle.load(fobj)

    # 计算当前所有文件的md5值
    for path, folders, files in os.walk(src_dir):
        for each_file in files:
            key = os.path.join(path, each_file)
            md5dict[key] = check_md5(key)

    # 更新md5字典
    with open(md5file, 'wb') as fobj:
        pickle.dump(md5dict, fobj)

    # 新增的文件和有变化的文件才进行备份
    tar = tarfile.open(fname, 'w:gz')
    for key in md5dict:
        if oldmd5.get(key) != md5dict[key]:
            tar.add(key)
    tar.close()


if __name__ == '__main__':
    src_dir = '/tmp/demo/security'
    dst_dir = '/var/tmp/backup'
    md5file = '/var/tmp/backup/md5.data'
    if strftime('%a') == 'Mon':
        full_backup(src_dir, dst_dir, md5file)
    else:
        incr_backup(src_dir, dst_dir, md5file)
